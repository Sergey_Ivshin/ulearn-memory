﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.IO;

namespace PCHMI
{
    class BitmapEx
    {
        public static void Main()
        {
            Bitmap bitmap;
            try {
                bitmap = (Bitmap)Bitmap.FromFile("cat.jpg");
            }
            catch (FileNotFoundException ex) {
                Console.WriteLine("FileNotFound. Creating new file.. Error log:"+ex);
                bitmap = new Bitmap(100, 100);
            }

            Timer time = new Timer();

            using (time.Start()) {
                Color color = bitmap.GetPixel(0, 1);
                Console.WriteLine("Color bitmap: {0}", color.Name);
            }
            Console.WriteLine("Bitmap.GetPixel: {0} msec", time.ElapsedMilliseconds);

            using (time.Start()) {
                using (var bitmapEditor = new BitmapEditor(bitmap))
                {
                    Color color = bitmapEditor.GetPixel(0, 1);
                    Console.WriteLine("Color bitmapEditor: {0}", color.Name);
                }
            }
            Console.WriteLine("bitmapEditor.GetPixel: {0} msec", time.ElapsedMilliseconds);

            using (time.Start())
            {
                bitmap.SetPixel(0, 1, Color.White);
                bitmap.Save("cat1.jpg");
            }
            Console.WriteLine("Bitmap.SetPixel: {0} msec", time.ElapsedMilliseconds);

            using (time.Start())
            {
                using (var bitmapEditor = new BitmapEditor(bitmap))
                {
                    bitmapEditor.SetPixel(0, 1, 255, 255, 255);
                    bitmap.Save("cat2.jpg");
                }
            }
            Console.WriteLine("bitmapEditor.SetPixel: {0} msec", time.ElapsedMilliseconds);

            bitmap.Dispose();
            Console.ReadKey();
        }
    }
}
