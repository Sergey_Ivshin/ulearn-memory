﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;

namespace PCHMI
{
    class BitmapEditor:IDisposable
    {
        private Bitmap bitmap;
        public BitmapEditor(Bitmap bitmap) {
            this.bitmap = bitmap;
        }
        public void Dispose()
        {
            //do something
        }
        public void SetPixel(int x, int y, byte colorR, byte colorG, byte colorB) {
            int PixelsCount = 1;
            Rectangle rect = new Rectangle(x, y, PixelsCount, PixelsCount);
            BitmapData bimapData =  bitmap.LockBits(rect, System.Drawing.Imaging.ImageLockMode.ReadWrite, System.Drawing.Imaging.PixelFormat.Format24bppRgb);
            IntPtr ptr = bimapData.Scan0;
            int numBytes = bimapData.Stride * bitmap.Height;
            int offset = x * bitmap.Width + y;
            byte[] rgbValues = new byte[numBytes];
            Marshal.Copy(ptr, rgbValues, 0, numBytes);
            rgbValues[offset] = colorB;
            rgbValues[offset + 1] = colorG;
            rgbValues[offset + 2] = colorR;
            Marshal.Copy(rgbValues, 0, ptr, numBytes);
            bitmap.UnlockBits(bimapData);
        }
        public Color GetPixel(int x, int y) {
            int PixelsCount = 1;
            Rectangle rect = new Rectangle(x, y, PixelsCount, PixelsCount);
            BitmapData bimapData = bitmap.LockBits(rect, System.Drawing.Imaging.ImageLockMode.ReadWrite, System.Drawing.Imaging.PixelFormat.Format24bppRgb);
            IntPtr ptr = bimapData.Scan0;
            int numBytes = bimapData.Stride * bitmap.Height;
            int offset = x * bitmap.Width + y;
            byte[] rgbValues = new byte[numBytes];
            Marshal.Copy(ptr, rgbValues, 0, numBytes);
            Color color = Color.FromArgb(rgbValues[offset + 1], rgbValues[offset], rgbValues[offset + 2]);
            bitmap.UnlockBits(bimapData);
            return color;
        }
    }
}
