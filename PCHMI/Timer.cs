﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace PCHMI
{
    class Timer
    {
        public TimeSpan ElapsedMilliseconds;

        public StartProcess Start()
        {
            ElapsedMilliseconds = TimeSpan.Zero;
            return new StartProcess(this);
        }
        public StartProcess Continue()
        {
            return new StartProcess(this);
        }
        public class StartProcess : IDisposable
        {
            readonly Timer writer;
            readonly Stopwatch stopWatch;
            public StartProcess(Timer writer)
            {
                this.writer = writer;
                stopWatch = new Stopwatch();
                stopWatch.Start();
            }
            public void Dispose()
            {

                writer.ElapsedMilliseconds += stopWatch.Elapsed;
                stopWatch.Stop();
            }
        }
    }
}
