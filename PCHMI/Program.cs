﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;
using System.Threading;

namespace PCHMI
{

    
    class Program
    {
        public static void Main1()
        {
            var timer = new Timer();
            using (timer.Start())
            {
                Thread.Sleep(1000);
            }
            Console.WriteLine(timer.ElapsedMilliseconds);

            using (timer.Continue())
            {
                Thread.Sleep(8000);
            }
            Console.WriteLine(timer.ElapsedMilliseconds);
            Console.ReadKey();
        }
        
    }

}

